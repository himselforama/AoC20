import re
validCount = 0
with open('input2.txt') as file_object:
    lines = file_object.readlines()
for line in lines:
    positionOne = re.split(r'\b',line)[1]
    positionTwo = re.split(r'\b',line)[3]
    character = re.split(r'\b',line)[5]
    sequence = re.split(r'\b',line)[7]
    if (sequence[int(positionOne)-1] == character) and (sequence[int(positionTwo)-1] != character):
      validCount += 1
    elif (sequence[int(positionOne)-1] != character) and (sequence[int(positionTwo)-1] == character):
      validCount += 1
print("Valid:",validCount)