import sys
with open('input1.txt') as numberFile:
    numbers = numberFile.read().splitlines()
for first in numbers:
    temp = 2020 - int(first)
    for second in numbers:
        temp2 = temp - int(second)
        for third in numbers:
            temp3 = temp2 - int(third)
            if temp3 == 0:
                result = int(first) * int(second) * int(third)
                print("first:",first,"second:" ,second, "third:",third)
                print(result)
                sys.exit