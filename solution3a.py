# linechars: 0-30
stepsize = 3
step = -stepsize
trees = 0
with open('input3.txt') as terrain:
    lines = terrain.read().splitlines()
for line in lines:
    step += stepsize
    if step <= 30:
        position = line[step]
    if step > 30:
        step = step - 31
        position = line[step]
    if position == '#':
        trees += 1
print("trees:",trees)