import sys
with open('input1.txt') as numberFile:
    numbers = numberFile.read().splitlines()
for first in numbers:
    second = 2020 - int(first)
    for temp in numbers:
        if second - int(temp) == 0:
            result = int(first) * second
            print(result)
            sys.exit()