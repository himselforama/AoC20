import re
validCount = 0
with open('input2.txt') as file_object:
    lines = file_object.readlines()
for line in lines:
    minAmount = re.split(r'\b',line)[1]
    maxAmount = re.split(r'\b',line)[3]
    character = re.split(r'\b',line)[5]
    sequence = re.split(r'\b',line)[7]
    analyze = re.findall(character,sequence)
    if (len(analyze) >= int(minAmount)) and (len(analyze) <= int(maxAmount)):
        validCount += 1
print(validCount)